# Account Login Prompt

# R-Auth Client Features Documentation - Account Login Prompt

## Table of Contents
1. [Account Login Prompt](#account-login-prompt)
2. [Troubleshooting](#troubleshooting)
3. [Support](#support)

## Account Login Prompt
The account login prompt in R-Auth allows for customization of the login experience. This section explains how to set up and manage the account login prompt.

### Configuring Account Login Prompt
1. Navigate to the **Clients** section in the R-Auth management console.
2. Select the client for which you want to configure the account login prompt.
3. In the **Account Login Prompt** section, customize the login prompt settings as needed.
4. Click **Save** to update the client settings.



## Troubleshooting
This section provides solutions to common issues faced while configuring account login prompts in R-Auth.

## Support
For additional help, contact R-Auth support -support@castlecraft.in
