
## R-Auth Documentation

R-Auth user and operations manual.

[Documentation](https://castlecraft.gitlab.io/rauth/docs)

## Contribute

Clone and switch to directory

```shell
git clone https://gitlab.com/castlecraft/rauth/docs.git
cd docs
```

### Using VSCode Devcontainers

Re-Open in devcontainer

```shell
cp -R devcontainer-example .devcontainer
```

### Setup and start

If not using devcontainer, python3, python3 venv and python3 pip is needed on your machine.

```shell
python3 -m venv env
. ./env/bin/activate
pip install -e .
mkdocs serve
```

Open http://127.0.0.1:8000/rauth/docs in browser.
